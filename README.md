# WebMU

A text-based world making tool built in react (front end) and node (back end). This is a monorepo containing both codebases. Commands should be familiar to anyone who has used a MUCK, but is quite a different beast architecturally.

Note: This codebase is formatted with [Prettier](https://prettier.io/).

**[Version: 0.0.0](https://semver.org/)**

- [Documentation](https://docs.foxriot.com)

- [Bug Report / Feature Request](https://gitlab.com/theplacelab/webmu/issues)

* [MIT Licensed](LICENSE), &copy; 2019 **[Place Lab Ltd.](https://theplacelab.com)**
