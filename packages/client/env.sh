#!/bin/bash
# Adapted from technique described here:
# https://medium.freecodecamp.org/how-to-implement-runtime-environment-variables-with-create-react-app-docker-and-nginx-7f9d42a91d70

ENV_CONFIG=$1

# Recreate config file
rm -rf $ENV_CONFIG
touch $ENV_CONFIG

# Add assignment
echo "// This is a generated file, do not edit directly" >> $ENV_CONFIG
echo "window._env_ = {" >> $ENV_CONFIG

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [[ -n "$line" ]];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    if [[ $line == *"REACT_APP_"* ]]; then
      varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
      varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')

        # Read value of current variable if exists as Environment variable
        value=$(printf '%s\n' "${!varname}")

        # Strip double double quotes
        varvalue=$(printf '%s' "$varvalue" | sed -e "s/\"//g")

        # Otherwise use value from .env file
        [[ -z $value ]] && value=${varvalue}

        # Append configuration property to JS file
        echo "  $varname: \"$value\"," >> $ENV_CONFIG
    fi

  fi

done < .env

echo "}" >> $ENV_CONFIG
