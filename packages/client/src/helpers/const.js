const constants = {
  ROLE: Object.freeze({
    WIZARD: 0,
    ADMIN: 1,
    PLAYER: 2,
    TOAD: 3,
    NOT_LOGGED_IN: 999
  }),

  ANON_HEADERS: {
    Accept: 'application/json, application/xml, text/plain, text/html, *.*',
    'Content-Type': 'application/json; charset=utf-8'
  },

  MSG_TYPE: Object.freeze({
    INFO: 'INFO',
    ACTION: 'ACTION'
  }),

  RETURN_DATA_TYPE: Object.freeze({
    JSON: 'JSON',
    HTML: 'HTML',
    LINK: 'LINK'
  }),

  TARGET: Object.freeze({
    INLINE: 'INLINE',
    OVERLAY: 'OVERLAY'
  }),
  EVENT: Object.freeze({
    POST_LOCAL_MESSAGE: 'POST_LOCAL_MESSAGE'
  }),

  MAX_BUFFER: 50000,

  MAX_MESSAGE_LENGTH: 867,

  MAX_COMMAND_HISTORY: 10,

  ACTION_DIRECTION: Object.freeze({
    INCOMING: 'INCOMING',
    OUTGOING: 'OUTGOING'
  }),

  TARGET_TYPE: Object.freeze({
    ALL: 'ALL',
    ROOM: 'ROOM',
    LINK: 'LINK',
    USER: 'USER'
  }),

  STOPWORDS: [
    'to',
    'through',
    'that',
    'the',
    'a',
    'an',
    'and',
    'or',
    'at',
    'for',
    'in'
  ],

  DIRECTIONAL: [
    'left',
    'right',
    'forward',
    'backward',
    'front',
    'back',
    'up',
    'down',
    'north',
    'south',
    'east',
    'west',
    'northeast',
    'southeast',
    'southwest',
    'northwest'
  ]
};
export default constants;
