import Mousetrap from 'mousetrap';
import * as helpers from '../helpers';

const mousetrapHelper = {
  mousetrapMount: ctx => {
    // Mousetrap unbindings
    Mousetrap.bind(['ctrl+l', 'meta+l'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'clearscreen');
      return false;
    });
    /*Mousetrap.bind(['esc'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'escape');
      return false;
    });*/
    Mousetrap.bind(['up'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'up_arrow');
      return false;
    });
    Mousetrap.bind(['down'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'down_arrow');
      return false;
    });
    Mousetrap.prototype.stopCallback = () => {
      return false;
    };
  },
  mousetrapUnmount: ctx => {
    Mousetrap.unbind(['ctrl+l', 'meta+l'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'clearscreen');
      return false;
    });
    Mousetrap.unbind(['esc'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'escape');
      return false;
    });
    Mousetrap.unbind(['up'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'up_arrow');
      return false;
    });
    Mousetrap.unbind(['down'], function(event) {
      mousetrapHelper.mousetrapHandler(ctx, event, 'down_arrow');
      return false;
    });
  },
  mousetrapHandler: (ctx, event, key) => {
    event.stopPropagation();
    switch (key) {
      case 'clearscreen':
        helpers.appView.clearScreen(ctx);
        break;
      case 'escape':
        ctx.onCloseOverlay(event);
        break;

      case 'up_arrow':
        if (ctx.commandHistoryPosition < 0) {
          ctx.commandHistoryPosition = ctx.commandHistory.length;
        }
        if (ctx.commandHistoryPosition > 0) {
          ctx.commandHistoryPosition--;
        }
        ctx.commandHistoryPosition =
          ctx.commandHistoryPosition < 0 ? 0 : ctx.commandHistoryPosition;
        if (ctx.commandHistory[ctx.commandHistoryPosition]) {
          ctx.onNewText(ctx.commandHistory[ctx.commandHistoryPosition]);
        }
        break;

      case 'down_arrow':
        if (ctx.commandHistoryPosition < 0) {
          ctx.commandHistoryPosition = ctx.commandHistory.length - 1;
        }
        if (ctx.commandHistoryPosition < ctx.commandHistory.length) {
          ctx.commandHistoryPosition++;
          if (ctx.commandHistory[ctx.commandHistoryPosition]) {
            ctx.onNewText(ctx.commandHistory[ctx.commandHistoryPosition]);
          }
        } else {
          ctx.commandHistoryPosition = -1;
          ctx.onNewText('');
        }
        break;

      default:
        break;
    }
  }
};
export default mousetrapHelper;
