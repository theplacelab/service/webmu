import linkifyUrls from 'linkify-urls';
import linewrap from 'linewrap';
import moment from 'moment';

const appViewHelper = {
  enableautoScrollEnabled: ctx => {
    setInterval(() => {
      if (ctx.autoScrollEnabled) {
        if (document.getElementById('chatbox') !== null) {
          document.getElementById(
            'chatbox'
          ).scrollTop = document.getElementById('chatbox').scrollHeight;
        }
      }
    }, 250);
  },

  ui_warningFlash: elementID => {
    var el = document.getElementById(elementID);
    el.classList.remove('warning_flash');
    el.classList.add('warning_flash');
    setTimeout(() => {
      el.classList.remove('warning_flash');
    }, 500);
  },

  ui_warningMore: elementID => {
    var el = document.getElementById(elementID);
    el.classList.remove('warning_more');
    el.classList.add('warning_more');
    setTimeout(() => {
      el.classList.remove('warning_more');
    }, 500);
  },

  onCloseOverlay: ctx => {
    ctx.setState({ overlayVisible: false });
  },

  onScrollChatBuffer: ctx => {
    ctx.autoScrollEnabled = false;
    var textArea = document.getElementById('chatbox');
    if (textArea.scrollHeight - textArea.scrollTop === textArea.clientHeight) {
      ctx.autoScrollEnabled = true;
      if (ctx.tempBuffer.length > 0) {
        ctx.setState({ chatBuffer: ctx.tempBuffer });
        ctx.tempBuffer = '';
      }
    }
  },

  clearScreen: ctx => {
    ctx.setState({ overlayVisible: false });
    ctx.setState({ chatBuffer: '' });
  },

  addToBuffer: (ctx, content) => {
    content = {
      content: '',
      wrap: true,
      linkify: true,
      timestamp: true,
      hr: false,
      ...content
    };

    let html = content.content;

    if (content.wrap) {
      let wrap = linewrap(80, {
        skipScheme: 'html',
        lineBreak: '\n'
      });
      html = wrap(html);
    }

    if (content.linkify) {
      html = linkifyUrls(html, {
        value: '[link]',
        attributes: {
          target: '_blank',
          rel: 'noopener',
          class: 'chat_link',
          one: 1,
          foo: true,
          multiple: ['a', 'b']
        }
      });
    }

    if (content.timestamp) {
      let timestamp = moment(Date.now()).isValid()
        ? `<span class='timestamp'>${moment(Date.now()).format(
            ctx.props.appState.dateFormat
          )}</span>`
        : '';
      if (content.hr) {
        html = `<div class="spacer"/>${timestamp} ${html}`;
      } else {
        html = `${timestamp} ${html}`;
      }
    }

    // Update the actual buffer
    if (ctx.autoScrollEnabled) {
      let updatedBuffer;
      if (ctx.tempBuffer.length > 0) {
        updatedBuffer = `${ctx.tempBuffer}<br>${html}`;
        ctx.tempBuffer = '';
      } else {
        updatedBuffer = `${ctx.state.chatBuffer}<br>${html}`;
      }

      // Update state with new buffer
      ctx.setState({ chatBuffer: updatedBuffer });
    } else {
      if (ctx.tempBuffer.length === 0) {
        ctx.tempBuffer = ctx.state.chatBuffer;
      }
      ctx.tempBuffer = ctx.tempBuffer + '\n' + html;
      appViewHelper.ui_warningMore('chatbox');
    }
  }
};

export default appViewHelper;
