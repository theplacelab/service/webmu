import CONST from './const.js';
import actionHelper from 'mu/actionHelper.js';

const appHelper = {
  parseInput: (input, muActions) => {
    if (!muActions) return;
    let text = input.trim();
    let object = text;
    let matchedAction = actionHelper.dummyAction_say;
    let params = {};

    let actionName = text.split(' ')[0].toLowerCase();
    muActions.forEach(thisAction => {
      let isCommand = thisAction.name.substr(0, 1) === '@';
      thisAction.name.split(',').forEach((alias, idx) => {
        if (actionName === `${isCommand && idx > 0 ? '@' : ''}${alias}`) {
          matchedAction = thisAction;
        }
      });
    });

    object = text.substr(actionName.length, text.length).trim();
    if (matchedAction.name.substr(0, 1) === '@') {
      if (object.includes('"')) {
        object = object.split('"');
        object = object.forEach(thisOption => {
          if (thisOption.length > 0) {
            console.log(thisOption);
            return thisOption;
          }
        });
        object = object.filter(thisOption => {
          return thisOption;
        });
      } else {
        object = object.split(' ');
      }
    } else {
      object = text.split(' ').filter(word => {
        if (!CONST.STOPWORDS.includes(word) && word !== actionName) {
          return word;
        }
        return null;
      });
      object = [object.join(' ')];
    }

    // Parse out the object if provided
    if (matchedAction.params) {
      let mapping = {};
      matchedAction.params.split(',').forEach((parameter, idx) => {
        mapping[parameter] = object[idx];
      });
      params = mapping;
    }

    // Special build-in aliases for emote and say command
    let firstChar = actionName.substr(0, 1);
    if (firstChar === '"' || firstChar === "'") {
      matchedAction = actionHelper.dummyAction_say;
      text = text.substr(1, text.length).trim();
      text = text.replace(/'|"$/, '');
    } else if (
      [':', '*'].includes(firstChar) ||
      actionName.substr(0, 3) === '/me'
    ) {
      matchedAction = actionHelper.dummyAction_emote;
      if (actionName.split(/\/me(.+)/).length > 1) {
        text = actionName.split('/me')[1];
      }
      if (actionName.split(/:(.+)/).length > 1) {
        text = actionName
          .split(':')
          .slice(1, actionName.split(':').length)
          .join(':');
      }
    }
    return { action: matchedAction, object, params, text };
  },

  recordHistory: (ctx, inputText) => {
    if (ctx.commandHistory[0] !== inputText) {
      ctx.commandHistory.push(inputText);
      ctx.commandHistoryPosition = -1;

      if (ctx.commandHistory.length >= CONST.MAX_COMMAND_HISTORY) {
        ctx.commandHistory = ctx.commandHistory.slice(
          ctx.commandHistory.length - CONST.MAX_COMMAND_HISTORY,
          ctx.commandHistory.length
        );
      }
    }
  }
};
export default appHelper;
