import CONST from './const.js';

const utility = {
  // Decodes a JWT token
  decodeToken: token => {
    return JSON.parse(atob(token.split('.')[1]));
  },

  escapeHtml: unsafe => {
    //.replace(/&/g, "&amp;")
    return unsafe
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#039;');
  },

  isLetter: str => {
    return str.length === 1 && str.match(/[a-z]/i);
  },

  consoleMessage: msg => {
    let event = new Event(CONST.EVENT.CONSOLE_MSG);
    event.message = msg;
    document.dispatchEvent(event);
  },

  incomingActionType: actionPacket => {
    if (
      typeof actionPacket.command !== 'undefined' &&
      actionPacket.command !== null
    ) {
      return 'command';
    } else if (
      typeof actionPacket.verb !== 'undefined' &&
      actionPacket.verb !== null
    ) {
      return 'verb';
    } else {
      return 'unknown';
    }
  },

  missingValue: variable => {
    if (!Array.isArray(variable)) {
      variable = [variable];
    }

    let isMissing = false;
    variable.forEach(value => {
      if (!isMissing) {
        isMissing =
          value === null ||
          typeof value === 'undefined' ||
          (typeof value === 'string' && value.trim().length === 0);
      }
    });
    return isMissing;
  }
};
export default utility;
