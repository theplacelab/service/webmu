import React, { Component } from 'react';

class Spinner extends Component {
  render() {
    return (
      <div
        id="spinner_container"
        className={
          this.props.isVisible
            ? 'spinner_container'
            : 'spinner_container hidden'
        }
      >
        <div className={this.props.error ? 'invisible' : 'spinner'}>
          <div className="spinner">
            <div className="bounce1" />
            <div className="bounce2" />
            <div className="bounce3" />
          </div>
        </div>
        <div className={this.props.error ? 'spinner error' : 'spinner hidden'}>
          <span
            className="fas fa-exclamation-triangle"
            style={{ fontSize: '5rem', color: 'white' }}
          />
        </div>
      </div>
    );
  }
}
export default Spinner;
