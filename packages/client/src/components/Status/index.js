import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Status.module.css';
import CONST from 'helpers/const.js';
class Status extends Component {
  render() {
    let roomInfo = `${this.props.serverState.world.name}: ${this.props.serverState.room.name} (${this.props.serverState.room.userCount}/${this.props.serverState.world.userCount})`;
    let className = styles.systemStatus + ' ';
    let statusText = this.props.connected ? roomInfo : '** OFFLINE **';
    if (
      this.props.parsedInput !== undefined &&
      Object.entries(this.props.parsedInput.action).length > 0
    ) {
      let params = '';
      for (let key in this.props.parsedInput.params) {
        key = key.trim();
        params += `[${key}]:${
          typeof this.props.parsedInput.params[key] === 'undefined'
            ? ''
            : this.props.parsedInput.params[key]
        } `;
      }

      let isCommand = this.props.parsedInput.action.name.substr(0, 1) === '@';
      if (this.props.parsedInput.action.name !== 'say') {
        statusText = `${this.props.parsedInput.action.name}: ${params}`;
        className += isCommand ? styles.verb : styles.command;
      }
    }
    return (
      <div
        id="command_console"
        className={`${styles.command_console}
         ${!this.props.connected ? styles.offline : ''}`}
      >
        <div className={className}>{statusText}</div>

        <div className={styles.userStatus}>
          {this.props.serverState.user.name}
          {this.props.serverState.user.role_id === CONST.ROLE.WIZARD && (
            <span className={styles.specialBits}>W</span>
          )}
        </div>
      </div>
    );
  }
}

Status.propTypes = {
  connected: PropTypes.bool.isRequired,
  serverState: PropTypes.object.isRequired,
  parsedInput: PropTypes.object
};

export default Status;
