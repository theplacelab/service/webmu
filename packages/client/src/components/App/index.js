import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as helpers from 'helpers';
import { Overlay, Spinner, Status } from '../';
import * as actions from 'state/actions';
import styles from './App.module.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fixedWidth: false,
      overlayVisible: false,
      overlayContent: '',
      textInput: '',
      chatBuffer: '',
      statusDisplay: { class: 'none', text: '' }
    };
    this.autoScrollEnabled = true;
    this.reachabilityTimer = '';
    this.tempBuffer = '';
    this.commandHistoryPosition = -1;
    this.commandHistory = [];
  }

  componentDidMount() {
    document.addEventListener(
      helpers.CONST.EVENT.POST_LOCAL_MESSAGE,
      this.onPostLocalMessage
    );
    helpers.mousetrap.mousetrapMount(this);
    helpers.appView.enableautoScrollEnabled(this);
    document.getElementById('inputField').focus();
  }

  componentWillUnmount() {
    document.removeEventListener(
      helpers.CONST.EVENT.POST_LOCAL_MESSAGE,
      this.onPostLocalMessage
    );
    helpers.mousetrap.mousetrapUnmount(this);
  }

  UNSAFE_componentWillUpdate() {
    let savedToken = sessionStorage.getItem('token');
    if (!this.props.appState.isLoggedIn && savedToken !== null) {
      sessionStorage.removeItem('token');
      this.props.dispatch(
        actions.dispatchMuAction({
          action: {
            description: '',
            id: null,
            name: 'reauthenticate',
            params: { token: savedToken },
            role_id: helpers.CONST.ROLE.NOT_LOGGED_IN
          }
        })
      );
    }
  }

  render() {
    let containerStyle = this.state.fixedWidth ? { width: '55rem' } : {};
    return (
      <div>
        <div className={styles.clientContainer} style={containerStyle}>
          <form onSubmit={this.onSubmit}>
            <div
              id="chatbox"
              className={styles.chatbox}
              readOnly={true}
              dangerouslySetInnerHTML={this.content()}
              onWheel={e => helpers.appView.onScrollChatBuffer(this, e)}
            />

            <Status
              connected={this.props.appState.isConnected}
              parsedInput={this.state.parsedInput}
              serverState={this.props.appState.server}
            />

            <div id="userInput" className={styles.terminal_input}>
              <div className={styles.prompt}>&gt;</div>
              <div>
                <input
                  id="inputField"
                  value={this.state.textInput}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </div>
            </div>
          </form>

          <Spinner isVisible={this.props.appState.showSpinner} />

          <Overlay
            hasCloseButton={false}
            onClose={this.onCloseOverlay}
            isVisible={this.state.overlayVisible}
            content={this.state.overlayContent}
          />
        </div>
      </div>
    );
  }

  onPostLocalMessage = event => {
    helpers.appView.addToBuffer(this, event.payload);
  };

  onChange = event => {
    this.onNewText(event.target.value);
  };

  onNewText = text => {
    if (text.length > helpers.CONST.MAX_MESSAGE_LENGTH) {
      helpers.appView.ui_warningFlash('userInput');
      return;
    }
    this.setState({
      textInput: text,
      parsedInput: helpers.app.parseInput(
        text,
        this.props.appState.server.world.actions
      )
    });
  };

  onSubmit = event => {
    event.preventDefault();
    event.stopPropagation();
    if (this.state.textInput.length === 0) return;
    helpers.app.recordHistory(this, this.state.textInput);
    this.props.dispatch(actions.dispatchMuAction(this.state.parsedInput));
    this.onNewText('');
    //FIXME: used for overlay document.getElementById('overlayComponent_content').scrollTop = 0;
  };

  content = () => {
    // Trim buffer
    if (this.state.chatBuffer.length > helpers.CONST.MAX_BUFFER) {
      this.setState({
        chatBuffer: this.state.chatBuffer.substr(
          this.state.chatBuffer.length - helpers.CONST.MAX_BUFFER,
          this.state.chatBuffer.length
        )
      });
    }
    return { __html: this.state.chatBuffer };
  };
}

function mapStateToProps(state) {
  return { appState: state.app, userState: state.user };
}

App.propTypes = {
  appState: PropTypes.object.isRequired,
  userState: PropTypes.object.isRequired,
  dispatch: PropTypes.func
};

export default connect(mapStateToProps)(App);
