import React, { Component } from 'react';
import { connect } from 'react-redux';
import WatchForOutsideClick from '../../components/WatchForOutsideClick';

class Overlay extends Component {
  render() {
    if (
      typeof this.props.content !== 'undefined' &&
      this.props.content.length > 0
    ) {
      return (
        <div
          id="overlayComponent"
          className={this.props.isVisible ? '' : 'hidden'}
        >
          <div id="overlayComponent_container">
            <div
              className={
                this.props.hasCloseButton ? 'control' : 'control invisible'
              }
            >
              <div onClick={this.props.onClose} className="closeButton">
                <span className="fas fa-times-circle" />
              </div>
            </div>
            <div id="overlayComponent_content" className="content">
              <WatchForOutsideClick onOutsideClick={this.props.onClose}>
                <div dangerouslySetInnerHTML={{ __html: this.props.content }} />
              </WatchForOutsideClick>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  return {
    appState: state.app,
    userState: state.user
  };
}

export default connect(mapStateToProps)(Overlay);
