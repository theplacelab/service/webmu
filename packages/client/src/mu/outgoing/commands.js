import CONST from '../../helpers/const.js';
import muActionHelper from '../actionHelper.js';
import moment from 'moment';

const commands = {
  default: (ap, as) => {
    return commands._passToServer(ap, as);
  },

  describe: (ap, as) => {
    return commands._actionWithSingleTarget(ap, as);
  },

  help: (ap, as) => {
    let content = '';
    let actionsArray = as.server.world.actions.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    if (ap.object.length > 0) {
      actionsArray.forEach(action => {
        if (action.name.includes(ap.object)) {
          content += `<span class="highlight">${action.name}</span> ${action.description}<br>`;
        }
      });
    } else {
      content = `Available actions:`;
      actionsArray.forEach(action => {
        content += `<div>${action.name.split(',')[0]}<div>`;
      });
    }
    muActionHelper.postLocalMessage({
      content,
      wrap: false,
      timestamp: false
    });
  },

  id: (ap, as) => {
    let targets = muActionHelper.findTargetInRoom(
      ap.params[ap.action.params],
      as.server
    );
    if (targets.matchesFound === 0) {
      muActionHelper.postLocalMessage({
        wrap: false,
        content: `'${targets.searchterm}' doesn't appear to be here.`
      });
    } else {
      let content = '';
      let label = '';
      targets.links.forEach(entity => {
        label = targets.matchesFound > 1 ? 'Exit ' : '';
        content += `${label}<span class='highlight'>#L${entity.id}</span> ${entity.name}`;
      });
      targets.rooms.forEach(entity => {
        label = targets.matchesFound > 1 ? 'Room ' : '';
        content += `${label}<span class='highlight'>#R${entity.id}:</span> ${entity.name}`;
      });
      targets.users.forEach(entity => {
        label = targets.matchesFound > 1 ? 'User ' : '';
        content += `${label}<span class='highlight'>#U${entity.id}:</span> ${entity.name}`;
      });
      return muActionHelper.postLocalMessage({ content });
    }
  },

  login: (ap, as) => {
    if (ap.object.length < 2) {
      return muActionHelper.postLocalMessage({
        wrap: false,
        content: ap.action.description
      });
    }
    return commands._passToServer(ap, as);
  },

  name: (ap, as) => {
    return commands._actionWithSingleTarget(ap, as);
  },

  _actionWithSingleTarget: (ap, as) => {
    let targets = muActionHelper.findTargetInRoom(
      ap.params['object'],
      as.server
    );
    if (targets.matchesFound === 0) {
      muActionHelper.postLocalMessage({
        wrap: false,
        content: `'${targets.searchterm}' doesn't appear to be here.`
      });
    } else if (targets.matchesFound > 1) {
      muActionHelper.postLocalMessage({
        wrap: false,
        content: `Which '${targets.searchterm}'? Try being more specific.`
      });
    } else {
      return commands._passToServer({ ...ap, targets: targets }, as);
    }
  },

  quit: (ap, as) => {
    sessionStorage.removeItem('token');
    return commands._passToServer(ap, as);
  },

  who: (ap, as) => {
    let connection = { ...as.server, socket: as.socket };
    if (typeof connection.world.users === 'undefined') return;
    let userList = `<div><span class="header">${connection.room.userCount}  HERE / ${connection.world.userCount} ONLINE</span></div>`;
    userList += `<table cellpadding=5>`;
    let roomlist = [];
    userList += `<tr><td>ENTITY</td><td>ARRIVED</td><td>ROOM</td></tr>`;
    connection.world.users.forEach(user => {
      let username =
        connection.user.name === user.name ? `${user.name} (you)` : user.name;
      userList += `<tr><td class="highlight">${username}</td><td>${moment(
        user.arrived_at
      ).fromNow()}</td><td class="highlight">${user.currentRoom.name}(${
        user.currentRoom.id
      })</td></tr>`;
      if (user.currentRoom.id === connection.room.id) {
        roomlist.push(username);
      }
    });
    userList += `<table>`;

    if (ap.params.option.length > 0) {
      if (roomlist.length > 0) {
        muActionHelper.postLocalMessage({
          content: `Here: ${roomlist.join(', ')}`,
          wrap: false,
          timestamp: false
        });
      } else {
        muActionHelper.postLocalMessage({
          content: `You are alone.`,
          wrap: false,
          timestamp: false
        });
      }
    } else {
      muActionHelper.postLocalMessage({
        content: userList,
        wrap: false,
        timestamp: false
      });
    }
  },

  _who_here: currentState => {
    return commands.who(
      {
        action: {
          id: 1,
          name: '@who',
          params: null,
          description: '',
          role_id: 999
        },
        object: ['here'],
        params: {},
        text: '@who here'
      },
      currentState
    );
  },

  _passToServer: (ap, as) => {
    let connection = { ...as.server, socket: as.socket };
    connection.socket.emit(CONST.MSG_TYPE.ACTION, {
      ...ap,
      token: as.server.user.token
    });
  }
};
export default commands;
