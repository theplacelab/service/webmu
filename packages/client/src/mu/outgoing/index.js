import command from './commands.js';
import verb from './verbs.js';

export const outgoingHandler = (actionPacket, appState) => {
  if (!appState.isConnected) return;
  let isCommand = actionPacket.action.name.substr(0, 1) === '@';
  let actionName = isCommand
    ? actionPacket.action.name
        .split(',')[0]
        .substr(1, actionPacket.action.name.split(',')[0].length)
    : actionPacket.action.name.split(',')[0];
  if (command[actionName]) {
    command[actionName](actionPacket, appState);
  } else if (verb[actionName]) {
    verb[actionName](actionPacket, appState);
  } else {
    //console.warn(`No outgoing handler for: '${actionName}'`);
    command['default'](actionPacket, appState);
  }
};
