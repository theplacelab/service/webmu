import mh from '../actionHelper.js';
import CONST from 'helpers/const.js';
import commands from 'mu/outgoing/commands.js';
import linewrap from 'linewrap';

const verbs = {
  go: (actionPacket, appState) => {
    let targets = mh.findTargetInRoom(
      actionPacket.params[actionPacket.action.params],
      appState.server,
      CONST.TARGET_TYPE.LINK
    );

    if (targets.matchesFound === 0) {
      let exitList = [];
      let connection = { ...appState.server, socket: appState.socket };
      connection.room.links.map(link => {
        return exitList.push(link.name.toUpperCase());
      });
      mh.postLocalMessage({
        content: `No exit named '${
          targets.searchterm
        }.' Try: <span class='highlight'>${JSON.stringify(exitList)}</span>`
      });
    } else if (targets.matchesFound === 1) {
      return commands._passToServer(
        {
          action: { name: 'teleport' },
          params: { id: targets.links[0].destination_id }
        },
        appState
      );
    } else {
      mh.postLocalMessage({
        wrap: false,
        content: `I'm not sure which '${targets.searchterm}'?`
      });
    }
    return;
  },

  look: (actionPacket, appState) => {
    let wrap = linewrap(80, {
      skipScheme: 'html',
      lineBreak: '\n'
    });
    if (actionPacket.params[actionPacket.action.params].length === 0) {
      actionPacket.params[actionPacket.action.params] = 'here';
    }
    let hr = false;
    let targets = mh.findTargetInRoom(
      actionPacket.params[actionPacket.action.params],
      appState.server
    );
    if (targets.matchesFound === 0) {
      mh.postLocalMessage({
        wrap: false,
        content: `'${targets.searchterm}' doesn't appear to be here.`
      });
    } else {
      let content = '';
      let label = '';

      targets.users.forEach(entity => {
        label = targets.matchesFound > 1 ? '<br>User: ' : '';
        content += wrap(
          `${label}<span class='highlight'>${entity.name}:</span> ${entity.description}`
        );
      });

      targets.links.forEach(entity => {
        label = targets.matchesFound > 1 ? '<br>Exit: ' : '';
        content += wrap(
          `${label}<span class='highlight'>${entity.name}:</span> ${entity.description}`
        );
      });

      targets.rooms.forEach(entity => {
        hr = true;
        label = targets.matchesFound > 1 ? '<br>Room: ' : '';
        let exits = '';
        let users = '';
        if (entity.links.length > 0) {
          let exitArray = [];
          entity.links.forEach(exit => {
            exitArray.push(exit.name);
          });
          let exitList = exitArray.join(', ');
          exits = `<br><span class='highlight'>Exits:</span> ${exitList}`;
          exits = wrap(exits);
        }
        if (entity.users.length > 0) {
          let userArray = [];
          entity.users.forEach(thisUser => {
            if (thisUser.id !== appState.server.user.id)
              userArray.push(thisUser.name);
          });
          if (userArray.length === 0) {
            users = `<br><span class='highlight'>Here:</span> You are alone.`;
          } else {
            users = `<br><span class='highlight'>Here:</span> ${userArray.join(
              ', '
            )}`;
          }
          users = wrap(users);
        }
        let desc = wrap(
          `You are in <span class='highlight'>${entity.name}:</span><br>${entity.description}`
        );
        content += `${desc} ${exits} ${users}`;
      });

      return mh.postLocalMessage({
        wrap: false,
        hr: hr,
        content
      });
    }
  },

  _look_here: currentState => {
    return verbs.look(
      {
        action: { name: 'look', params: 'object' },
        params: { object: 'here' }
      },
      currentState
    );
  }
};
export default verbs;
