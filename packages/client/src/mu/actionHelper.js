import CONST from 'helpers/const.js';
import { incomingHandler } from 'mu/incoming';
import { outgoingHandler } from 'mu/outgoing';

const muActionHelper = {
  handleMuAction: (direction, actionPacket, appState) => {
    if (typeof actionPacket.action === 'undefined') return;
    if (appState.server.user.role_id > actionPacket.action.role_id) {
      muActionHelper.postLocalMessage({
        content: `[${actionPacket.action.name}] requires permission. Are you logged in?`
      });
    }
    if (direction === CONST.ACTION_DIRECTION.INCOMING) {
      incomingHandler(actionPacket, appState);
    } else {
      outgoingHandler(actionPacket, appState);
    }
  },

  postLocalMessage: content => {
    let event = new CustomEvent(CONST.EVENT.POST_LOCAL_MESSAGE);
    event.payload = content;
    document.dispatchEvent(event);
  },

  findTargetInRoom: (
    target = 'here',
    source,
    scope = CONST.TARGET_TYPE.ALL
  ) => {
    let matches = {
      searchterm: target,
      matchesFound: 0,
      users: [],
      rooms: [],
      links: []
    };
    if (typeof target !== 'undefined') {
      target = target.trim().toLowerCase();
      if (target === 'me') {
        matches.matchesFound++;
        matches.users.push(source.user);
      } else if (target === 'here') {
        matches.matchesFound++;
        matches.rooms.push(source.room);
      } else {
        if (
          scope === CONST.TARGET_TYPE.USER ||
          scope === CONST.TARGET_TYPE.ALL
        ) {
          source.room.users.forEach(user => {
            if (user.name.toLowerCase().includes(target)) {
              matches.matchesFound++;
              matches.users.push(user);
            }
          });
        }
        if (
          scope === CONST.TARGET_TYPE.LINK ||
          scope === CONST.TARGET_TYPE.ALL
        ) {
          source.room.links.forEach(link => {
            if (link.name.toLowerCase().includes(target)) {
              matches.matchesFound++;
              matches.links.push(link);
            }
          });
        }
      }
    }
    return matches;
  },

  dummyAction_say: {
    description: '',
    id: null,
    name: 'say',
    params: '',
    role_id: CONST.ROLE.NOT_LOGGED_IN
  },

  dummyAction_emote: {
    description: '',
    id: null,
    name: 'emote',
    params: '',
    role_id: CONST.ROLE.NOT_LOGGED_IN
  }
};
export default muActionHelper;
