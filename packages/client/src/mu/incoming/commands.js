import muActionHelper from 'mu/actionHelper.js';
import utility from 'helpers/utility.js';
import moment from 'moment';

const commands = {
  say: actionPacket => {
    let sender = actionPacket.sender_is_me
      ? 'You say:'
      : `${actionPacket.sender.name} says:`;
    muActionHelper.postLocalMessage({
      content: `${sender} <span class="highlight">${actionPacket.text}</span>`
    });
  },

  emote: actionPacket => {
    let text = !utility.missingValue(actionPacket.sender.name)
      ? `${actionPacket.sender.name} ${actionPacket.text}`
      : actionPacket.text;
    muActionHelper.postLocalMessage({ content: text });
  },

  do_url: actionPacket => {
    window.open(actionPacket.params.url);
  },

  do_getBackup: (actionPacket, appState) => {
    fetch(actionPacket.params.url, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + appState.server.user.token,
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
      .then(response => response.blob())
      .then(blob => {
        // 2. Create blob link to download
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `database-${moment().toISOString()}.db`);
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      });
  }
};

export default commands;
