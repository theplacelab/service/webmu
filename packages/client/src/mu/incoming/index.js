import command from './commands.js';
import verb from './verbs.js';

export const incomingHandler = (actionPacket, appState) => {
  let isCommand = actionPacket.action.name.substr(0, 1) === '@';
  let actionName = isCommand
    ? actionPacket.action.name
        .split(',')[0]
        .substr(1, actionPacket.action.name.split(',')[0].length)
    : actionPacket.action.name.split(',')[0];
  if (command[actionName]) {
    command[actionName](actionPacket, appState);
  } else if (verb[actionName]) {
    verb[actionName](actionPacket, appState);
  } else {
    console.error(`No incoming handler for: '${actionName}'`);
  }
};

// Fires after a new info packet has come in
export const infoPostHandler = (actionPacket, previousState, currentState) => {
  currentState.isLoggedIn = currentState.server.room.id !== 0;
  // Store the JWT
  if (((actionPacket || {}).user || {}).token) {
    sessionStorage.setItem('token', actionPacket.user.token);
  }
};
