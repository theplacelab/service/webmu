import outgoing_verbs from 'mu/outgoing/verbs.js';
const verbs = {
  do_look: (actionPacket, appState) => {
    outgoing_verbs.look(
      {
        action: { name: 'look', params: 'object' },
        params: { object: 'here' }
      },
      appState
    );
  }
};
export default verbs;
