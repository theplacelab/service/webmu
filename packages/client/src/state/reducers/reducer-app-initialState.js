export default {
  isConnected: false,
  isLoggedIn: false,
  hasError: false,
  dateFormat: 'h:MM:ssa',
  dateFormat_long: 'DD/MM/YY @ h:MM:ssa',
  ui: {
    autoScrollEnabled: true,
    bufferMaxChars: 10
  },
  server: {
    world: {
      userCount: 0,
      name: ''
    },
    room: {
      userCount: 0,
      users: [],
      name: '',
      description: ''
    },
    user: {
      id: '',
      ip: '',
      name: '',
      description: '',
      arrived_at: Date.now()
    }
  }
};
