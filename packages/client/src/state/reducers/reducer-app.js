import initialState from './reducer-app-initialState';
import * as actionTypes from 'state/actions/action-types';

export default function api(state = initialState, action) {
  switch (action.type) {
    case actionTypes.APP_MU_UPDATE: {
      let payload = action.payload.payload;
      return {
        ...state,
        server: {
          ...state.server,
          room: { ...state.server.room, ...payload.room },
          world: { ...state.server.world, ...payload.world },
          user: { ...state.server.user, ...payload.user }
        }
      };
    }

    case actionTypes.APP_SETTINGS_UPDATE: {
      return {
        ...state,
        ...action.payload
      };
    }

    case actionTypes.ERROR: {
      return {
        ...state,
        hasError: true
      };
    }

    default:
      return state;
  }
}
