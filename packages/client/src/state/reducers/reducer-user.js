import initialState from './reducer-user-initialState';

export default function api(state = initialState, action) {
  switch (action.type) {
    default: {
      return state;
    }
  }
}
