import { combineReducers } from 'redux';
import app from './reducer-app';
import user from './reducer-user';

const rootReducer = combineReducers({
  app,
  user
});

export default rootReducer;
