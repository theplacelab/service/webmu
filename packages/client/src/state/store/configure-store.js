import createSagaMiddleware from 'redux-saga';
import appSagas from 'state/sagas/app';
import socketSagas from 'state/sagas/socket';
import rootReducer from 'state/reducers';
import { createStore, applyMiddleware } from 'redux';

const appSagaMW = createSagaMiddleware();
const socketSagaMW = createSagaMiddleware();

export default () => {
  let store = createStore(
    rootReducer,
    applyMiddleware(appSagaMW, socketSagaMW)
  );
  appSagaMW.run(appSagas);
  socketSagaMW.run(socketSagas);
  return store;
};
