////////////////////////////////////////////////////////////////////////////////
// client.js
// Sagas for client UI
////////////////////////////////////////////////////////////////////////////////
import CONST from 'helpers/const.js';
import { put, select } from 'redux-saga/effects';
import * as actions from 'state/actions/action-types';
import utility from 'helpers/utility.js';
import actionHelper from 'mu/actionHelper.js';

export const getAppState = state => state.app;

export const dispatchMuAction = function* onError(action) {
  let appState = yield select(getAppState);
  actionHelper.handleMuAction(
    CONST.ACTION_DIRECTION.OUTGOING,
    action.payload,
    appState
  );
};

export const onError = function* onError(action) {
  console.error(action);
  yield utility.consoleMessage(action.payload.error.message);
};

// Disconnect
export const deAuthenticate = function* deAuthenticate() {
  //yield call(helpers.socket_api.disconnect);
};

// Response Received (from both login and reauth)
export const responseReceived = function* responseReceived(action) {
  // Rejected
  if (typeof action.payload[0] === 'undefined') {
    yield put({
      type: actions.ERROR,
      payload: action.payload
    });
  }
};
