import { takeLatest, all } from 'redux-saga/effects';
import * as actions from 'state/actions/action-types';

import * as app from './app';

export default function* rootSaga() {
  yield all([
    // App
    takeLatest(actions.APP_DISPATCH_MU_ACTION, app.dispatchMuAction),
    takeLatest(actions.ERROR, app.onError)
  ]);
}
