import { eventChannel } from 'redux-saga';
import { take, takeLatest, call, put, all } from 'redux-saga/effects';
import * as actions from 'state/actions/action-types';
import * as socket from './socket';
import CONST from 'helpers/const.js';
import io from 'socket.io-client';

export default function* socketIOSagas() {
  yield all([
    takeLatest(actions.SOCKET_MESSAGE_ACTION, socket.actionReceived),
    takeLatest(actions.SOCKET_MESSAGE_INFO, socket.infoReceived),
    takeLatest(actions.SOCKET_CONNECTED, socket.connected),
    takeLatest(actions.SOCKET_DISCONNECTED, socket.disconnected)
  ]);
  const channel = yield call(() => socketIO_init());
  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}

function socketIO_init() {
  return eventChannel(emitter => {
    try {
      let socket = io(window._env_.REACT_APP_SOCKET_SERVER, {
        transports: ['websocket', 'polling']
      });
      socket.on(CONST.MSG_TYPE.ACTION, payload => {
        return emitter({ type: actions.SOCKET_MESSAGE_ACTION, payload });
      });
      socket.on(CONST.MSG_TYPE.INFO, payload => {
        return emitter({
          type: actions.SOCKET_MESSAGE_INFO,
          payload: payload.payload
        });
      });
      socket.on('connect', () => {
        return emitter({ type: actions.SOCKET_CONNECTED, payload: socket });
      });
      socket.on('disconnect', () => {
        return emitter({ type: actions.SOCKET_DISCONNECTED, payload: socket });
      });
    } catch (e) {
      console.error(`SOCKET FAILED: ${e}`);
    }
    return () => {
      console.log('Socket Closed');
    };
  });
}
