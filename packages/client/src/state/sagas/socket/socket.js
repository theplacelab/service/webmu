import { put, select } from 'redux-saga/effects';
import CONST from 'helpers/const.js';
import actionHelper from 'mu/actionHelper.js';
import * as incoming from 'mu/incoming';
import * as actionType from 'state/actions/action-types';

export const getAppState = state => state.app;

export const actionReceived = function*(action) {
  let incomingAction = action.payload.message;
  let appState = yield select(getAppState);
  incomingAction = {
    ...incomingAction,
    sender_is_me:
      action.payload.message.sender.name === appState.server.user.name
  };
  actionHelper.handleMuAction(
    CONST.ACTION_DIRECTION.INCOMING,
    incomingAction,
    appState
  );
};

export const infoReceived = function*(action) {
  let previousAppState = yield select(getAppState);
  yield put({
    type: actionType.APP_MU_UPDATE,
    payload: { payload: action.payload }
  });
  let currentAppState = yield select(getAppState);
  yield incoming.infoPostHandler(
    action.payload,
    previousAppState,
    currentAppState
  );
};

export const connected = function*(action) {
  yield put({
    type: actionType.APP_SETTINGS_UPDATE,
    payload: { isConnected: true, socket: action.payload }
  });
};

export const disconnected = function*(action) {
  yield put({
    type: actionType.APP_SETTINGS_UPDATE,
    payload: { isConnected: false, socket: action.payload }
  });
};
