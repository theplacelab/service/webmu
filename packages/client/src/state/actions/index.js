import * as actionType from 'state/actions/action-types';
function action(type, payload = {}) {
  return { type, payload };
}

export const dispatchMuAction = payload => action(actionType.APP_DISPATCH_MU_ACTION, payload);
