require('app-module-path').addPath(__dirname);
require('dotenv').config();
const app = require('express')();
const http = require('http').Server(app);
const server = require('helpers/server.js');
const io = require('socket.io')(http, { transports: ['websocket', 'polling'] });
const CONST = require('const.js');
const sqlite = require('better-sqlite3');
const actionHelper = require('helpers/action');
const socketHelper = require('helpers/socket.js');
const dbMgr = require('helpers/db');

//////////////////////////////////////////////////////////

dbMgr.db = sqlite('./database/webmu.db', { fileMustExist: true });
dbMgr.initialConfig = dbMgr.loadInitialConfig(dbMgr.db);

server.httpServer = http.listen(process.env.PORT, server.consoleWelcome(dbMgr));
server.setup(app);

io.on('connection', socket => {
  let connection = {
    io,
    dbMgr,
    socket
  };
  socket.config = socketHelper.onConnect(connection);
  socket.config.user.currentRoom = socket.config.room;
  actionHelper.moveToRoom(connection, socket.config.user.currentRoom, true);
  socket.on(CONST.MSG_TYPE.ACTION, actionPacket =>
    actionHelper.onIncomingAction(connection, actionPacket)
  );
  socket.on('disconnect', () => socketHelper.onDisconnect(connection));
});

server.exitHandler(process, dbMgr, server);
