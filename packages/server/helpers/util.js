const generator = require('generate-password');
const shell = require('shelljs');
require('app-module-path').addPath(__dirname);

module.exports = {
  generatePassword: length => {
    return generator.generate({
      length: length,
      numbers: true
    });
  },
  missingValue: variable => {
    if (!Array.isArray(variable)) {
      variable = [variable];
    }

    let isMissing = false;
    variable.forEach(value => {
      if (!isMissing) {
        isMissing =
          value === null ||
          typeof value === 'undefined' ||
          (typeof value === 'string' && value.trim().length === 0);
      }
    });
    return isMissing;
  },
  utf8ArrayToStr: array => {
    // http://www.onicos.com/staff/iz/amuse/javascript/expert/utf.txt

    /* utf.js - UTF-8 <=> UTF-16 convertion
     *
     * Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
     * Version: 1.0
     * LastModified: Dec 25 1999
     * This library is free.  You can redistribute it and/or modify it.
     */
    let out, i, len, c;
    let char2, char3;

    out = '';
    len = array.length;
    i = 0;
    while (i < len) {
      c = array[i++];
      switch (c >> 4) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
          // 0xxxxxxx
          out += String.fromCharCode(c);
          break;
        case 12:
        case 13:
          // 110x xxxx   10xx xxxx
          char2 = array[i++];
          out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
          break;
        case 14:
          // 1110 xxxx  10xx xxxx  10xx xxxx
          char2 = array[i++];
          char3 = array[i++];
          out += String.fromCharCode(
            ((c & 0x0f) << 12) | ((char2 & 0x3f) << 6) | ((char3 & 0x3f) << 0)
          );
          break;
      }
    }
    return out;
  },

  backupDatabase: cb => {
    let baseDir = __dirname + '/../database';
    let databaseFile = `${baseDir}/webmu.db`;
    let backupFile = `${baseDir}/backup.db`;
    shell.exec(`sqlite3 ${databaseFile} ".backup ${backupFile}"`, cb);
  },

  oppositeOf: input => {
    let direction_lut = {
      north: 'south',
      n: 's',
      east: 'west',
      e: 'w',
      north_east: 'south_west',
      ne: 'sw',
      north_west: 'south_east',
      nw: 'se',
      up: 'down',
      left: 'right',
      in: 'out'
    };

    Object.keys(direction_lut).forEach(key => {
      direction_lut[direction_lut[key]] = key;
    });
    const setCharAt = (str, index, chr) => {
      if (index > str.length - 1) return str;
      return str.substr(0, index) + chr + str.substr(index + 1);
    };
    let opposite = direction_lut[input.toLowerCase().trim()];
    if (!opposite) return input;
    let upCount = 0;
    for (var i = 0; i < (input.length < 3 ? input.length : 3); i++) {
      if (input.charAt(i) === input.charAt(i).toUpperCase()) {
        opposite = setCharAt(opposite, i, opposite.charAt(i).toUpperCase());
        upCount++;
      }
      if (upCount === 3) {
        opposite = opposite.toUpperCase();
      }
    }
    return opposite;
  }
};
