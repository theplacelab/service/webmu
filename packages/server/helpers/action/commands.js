const CONST = require('const.js');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const util = require('helpers/util.js');
const database = require('helpers/db');
const socketHelper = require('helpers/socket.js');

module.exports = {
  backup: c => {
    util.backupDatabase((code, stdout, err) => {
      if (!err) {
        return c.actionHelper._user_action(c, {
          action: { name: '@do_getBackup' },
          params: { url: process.env.BACKUP_URL }
        });
      }
    });
  },

  bug: c => {
    return c.actionHelper._user_action(c, {
      action: { name: '@do_url' },
      params: { url: process.env.BUG_REPORT_URL }
    });
  },

  describe: (c, actionPacket) => {
    let target = c.actionHelper._findTarget(actionPacket.targets);
    return module.exports._parameterChange(
      c,
      target,
      'description',
      actionPacket.object.slice(1, actionPacket.object.length).join(' '),
      `Description of ${target.target.name} has been changed.`
    );
  },

  dig: (c, actionPacket) => {
    if (
      util.missingValue(actionPacket.params.exit) ||
      util.missingValue(actionPacket.params.room)
    )
      return;
    const msg = content => c.actionHelper._user_announce(c, content);
    let exitName = actionPacket.params.exit.trim();
    let reciprocalExitName = util.oppositeOf(exitName);
    let db = c.dbMgr.initialConfig.db;
    let isWizard = database.user.isWizard(db, c.socket.config.user.id);
    let isOwner = c.actionHelper._doesOwn(c, {
      type: CONST.TARGET_TYPE.ROOM,
      target: c.socket.config.user.currentRoom
    });

    if (!(isOwner || isWizard)) {
      msg(`You cannot dig from a room you don't own.`);
    } else {
      let targets = c.actionHelper._findTargetInRoom(
        c,
        exitName.toLowerCase(),
        CONST.TARGET_TYPE.LINK
      );

      if (targets.matchesFound >= 1 && targets.exactMatch) {
        msg(
          `There is already a link named '${targets.searchterm},' which is going to be confusing.`
        );
      } else {
        let newRoomID = database.room.create(db, {
          name: actionPacket.params.room.trim(),
          owner_id: c.socket.config.user.id
        });
        database.link.create(db, {
          name: exitName,
          description: 'A rough dirt tunnel',
          location_id: c.socket.config.user.currentRoom.id,
          destination_id: newRoomID,
          owner_id: c.socket.config.user.id
        });
        c.actionHelper._reconnectEveryoneInRoom(
          c,
          c.socket.config.user.currentRoom,
          CONST.DO_QUIETLY
        );
        let previousRoomID = c.socket.config.user.currentRoom.id;
        c.actionHelper.moveToRoom(
          c,
          database.room.getById(db, newRoomID),
          false,
          () => {
            database.link.create(db, {
              name: reciprocalExitName,
              description: 'A rough dirt tunnel',
              location_id: c.socket.config.user.currentRoom.id,
              destination_id: previousRoomID,
              owner_id: c.socket.config.user.id
            });
            c.actionHelper._reconnectEveryoneInRoom(
              c,
              c.socket.config.user.currentRoom,
              CONST.DO_QUIETLY
            );
          }
        );
      }
    }
  },

  do_look: c => {
    return c.actionHelper._user_action(c, {
      action: { name: 'do_look' }
    });
  },

  emote: (c, actionPacket) => {
    return c.actionHelper._room_action(c, actionPacket);
  },

  eject: (c, actionPacket) => {
    const msg = content => c.actionHelper._user_announce(c, content);
    if (!actionPacket.params.username) return;
    let db = c.dbMgr.initialConfig.db;
    let isWizard = database.user.isWizard(db, c.socket.config.user.id);
    let isOwner = c.actionHelper._doesOwn(c, {
      type: CONST.TARGET_TYPE.ROOM,
      target: c.socket.config.user.currentRoom
    });
    if (!(isWizard || isOwner)) {
      return msg(`You cannot eject people from rooms you don't own.`);
    } else {
      let targets = c.actionHelper._findTargetInRoom(
        c,
        actionPacket.params.username.toLowerCase(),
        CONST.TARGET_TYPE.USER
      );
      if (targets.matchesFound === 0) {
        return msg(`Cannot find '${actionPacket.params.username}'`);
      } else if (targets.matchesFound > 1) {
        return msg(
          `'${actionPacket.params.username}' is ambiguous, tell me more`
        );
      } else if (targets.matchesFound === 1) {
        let target = c.actionHelper._findTarget(targets);
        if (target.target.id === c.socket.config.user.id) {
          msg(`You can't eject yourself!`);
        } else {
          msg(`Ejecting (${target.target.name})...`);
          let targetUserConnection = {
            ...c,
            socket: c.io.nsps['/'].adapter.nsp.sockets[target.target.socketID]
          };
          c.actionHelper._user_announce(
            targetUserConnection,
            'You have been ejected!'
          );
          c.actionHelper.moveToRoom(targetUserConnection, target.target.home);
        }
      }
    }
  },

  home: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    let defaultHome = database.room.getById(db, 1);
    if (
      typeof actionPacket.params !== 'undefined' &&
      typeof actionPacket.params.object !== 'undefined' &&
      actionPacket.params.action.toLowerCase().trim() === 'set' &&
      actionPacket.params.object === 'here'
    ) {
      c.actionHelper._user_announce(c, 'Your home has been set to here!');
      database.user.update(db, c.socket.config.user.id, {
        home: c.socket.config.user.currentRoom.id
      });
      c.socket.config.user.home = c.socket.config.user.currentRoom;
      return;
    }
    let user = c.socket.config.user;
    if (!user.home) {
      user.home = defaultHome;
    }
    if (user.home.id === user.currentRoom.id) {
      c.actionHelper._user_announce(c, 'You are already home!');
    } else {
      c.actionHelper._user_announce(
        c,
        `Sending you home (${user.home.name})...`
      );
      return c.actionHelper.moveToRoom(c, user.home);
    }
  },

  link: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    const msg = content => c.actionHelper._user_announce(c, content);
    let isRoomOwner = c.actionHelper._doesOwn(c, {
      type: CONST.TARGET_TYPE.ROOM,
      target: c.socket.config.user.currentRoom
    });
    let isWizard = database.user.isWizard(db, c.socket.config.user.id);
    let destinationDBID = parseInt(
      actionPacket.params.destination.match(/(\d+)$/)[0],
      10
    );

    if (!(isWizard | isRoomOwner)) {
      msg(`You cannot modify links in a room you don't own.`);
    } else {
      let action = actionPacket.params.action.toLowerCase().trim();
      let targets = c.actionHelper._findTargetInRoom(
        c,
        actionPacket.params.name.toLowerCase().trim(),
        CONST.TARGET_TYPE.LINK
      );
      switch (action) {
        case 'create':
        case 'c':
          if (targets.matchesFound >= 1 && targets.exactMatch) {
            msg(
              `There is already a link named '${targets.searchterm},' which could get confusing.`
            );
            return;
          } else {
            if (!destinationDBID) {
              msg(`Destination must be a valid DBID.`);
              return;
            }
            database.link.create(db, {
              name: actionPacket.params.name,
              description: 'A Door',
              location_id: c.socket.config.user.currentRoom.id,
              destination_id: destinationDBID,
              owner_id: c.socket.config.user.id
            });
            c.actionHelper._reconnectEveryoneInRoom(
              c,
              c.socket.config.user.currentRoom,
              CONST.DO_QUIETLY
            );
            msg(`Link '${targets.searchterm}' created!`);
          }
          break;

        case 'delete':
        case 'd':
          if (targets.matchesFound === 0) {
            msg("I don't see that here");
          } else if (targets.matchesFound > 1) {
            msg(
              `I'm not sure which '${targets.searchterm}' you mean, try being more specific.`
            );
          } else {
            let target = c.actionHelper._findTarget(targets);
            database.link.destroy(db, target.target.id);
            c.actionHelper._reconnectEveryoneInRoom(
              c,
              c.socket.config.user.currentRoom,
              CONST.DO_QUIETLY
            );
            msg(`Link '${targets.searchterm}' destroyed!`);
          }
          break;

        case 'update':
        case 'relink':
        case 'u':
          if (targets.matchesFound === 0) {
            msg("I don't see that here");
          } else if (targets.matchesFound > 1) {
            msg(
              `I'm not sure which '${targets.searchterm}' you mean, try being more specific.`
            );
          } else {
            if (!destinationDBID) {
              msg(`Destination must be a valid DBID.`);
              return;
            }
            let target = c.actionHelper._findTarget(targets);
            database.link.update(db, target.target.id, {
              destination_id: destinationDBID
            });
            c.actionHelper._reconnectEveryoneInRoom(
              c,
              c.socket.config.user.currentRoom,
              CONST.DO_QUIETLY
            );
            msg(`Link '${targets.searchterm}' updated!`);
          }

          break;

        default:
          return;
      }
    }
  },

  list: (c, actionPacket) => {
    const msg = content => c.actionHelper._user_announce(c, content);
    let db = c.dbMgr.initialConfig.db;
    let action = actionPacket.params.action.toLowerCase().trim();
    let targetList;
    if (actionPacket.params.list) {
      targetList = database.list.exists(
        db,
        actionPacket.params.list.toLowerCase().trim(),
        c.socket.config.user.id
      );
    }
    let nameOnList = () => false;
    if (targetList) {
      let memberList = database.list.members(db, targetList.id).members;
      nameOnList = userName => {
        return memberList.find(member => {
          return (
            member.name.toLowerCase().trim() === userName.toLowerCase().trim()
          );
        });
      };
    }

    if (!action) {
      let lists = database.list
        .getAllForOwner(db, c.socket.config.user.id)
        .map(item => {
          return item.name;
        })
        .join(', ');
      msg(`<span class='highlight'>Your Lists:</span> ${lists}`);
      return;
    }

    switch (action) {
      case 'create':
      case 'c':
        if (targetList) {
          msg(`List '${targetList.name}' already exists!`);
        } else {
          database.list.create(db, {
            name: actionPacket.params.list.toLowerCase().trim(),
            owner_id: c.socket.config.user.id
          });
          msg(`List "${actionPacket.params.list}" created.`);
        }
        break;
      case 'delete':
      case 'd':
        if (!targetList) {
          msg(`List "${targetList.name}" doesn't exists!`);
        } else {
          database.list.destroy(
            db,
            actionPacket.params.list.toLowerCase().trim(),
            c.socket.config.user.id
          );
          msg(`List "${targetList.name}" destroyed.`);
        }
        break;
      case 'add':
      case 'a':
        if (!targetList) {
          msg(`List "${actionPacket.params.list}" doesn't exists!`);
        } else {
          let targetUser = database.user.getByName(
            db,
            actionPacket.params.user.toLowerCase().trim()
          );
          if (!targetUser) {
            msg(`Not sure who "${actionPacket.params.user}" is."`);
          } else {
            if (!nameOnList(targetUser.name)) {
              database.list.addMember(db, targetList.id, targetUser.id);
              msg(
                `"${actionPacket.params.user}" added to "${targetList.name}"`
              );
            } else {
              msg(
                `"${actionPacket.params.user}" already on "${targetList.name}".`
              );
            }
          }
        }
        break;
      case 'remove':
      case 'r':
        if (!targetList) {
          msg(`List "${actionPacket.params.list}" doesn't exists!`);
        } else {
          let targetUser = database.user.getByName(
            db,
            actionPacket.params.user.toLowerCase().trim()
          );
          if (!targetUser) {
            msg(`Not sure who "${actionPacket.params.user}" is."`);
          } else {
            if (!nameOnList(targetUser.name)) {
              msg(`"${actionPacket.params.user}" not on "${targetList.name}".`);
            } else {
              database.list.removeMember(db, targetList.id, targetUser.id);
              msg(
                `"${actionPacket.params.user}" removed from "${targetList.name}"`
              );
            }
          }
        }
        break;
      case 'who':
      case 'w':
        {
          if (targetList) {
            let memberList = database.list
              .members(db, targetList.id)
              .members.map(item => {
                return item.name;
              })
              .join(', ');
            msg(
              `<span class="highlight">${actionPacket.params.list}:</span> ${
                memberList.length === 0 ? '*empty*' : memberList
              }`
            );
          } else {
            msg(`List "${actionPacket.params.list}" doesn't exists!`);
          }
        }
        break;
      default:
        return;
    }
  },

  login: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    c.dbMgr.authenticate(db, actionPacket.params, arrivingUser => {
      if (!arrivingUser) {
        return c.actionHelper._user_announce(c, 'Invalid credentials!');
      } else {
        return module.exports._onValidlogin(c, arrivingUser);
      }
    });
  },

  name: (c, actionPacket) => {
    let newName = c.actionHelper._parseName(actionPacket.params['name']);
    let target = c.actionHelper._findTarget(actionPacket.targets);
    if (
      target.type === CONST.TARGET_TYPE.USER &&
      database.user.exists(c.dbMgr.initialConfig.db, newName)
    ) {
      c.actionHelper._user_announce(c, `'${newName}' is already in use!`);
    } else {
      return module.exports._parameterChange(
        c,
        target,
        'name',
        newName,
        `'${target.target.name}' is henceforth known as '${newName}.'`
      );
    }
  },

  room: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    const msg = content => c.actionHelper._user_announce(c, content);
    let isWizard = database.user.isWizard(db, c.socket.config.user.id);
    let action = actionPacket.params.action.toLowerCase().trim();
    let targetID;
    let isOwner;
    let targetRoom;
    if (!(action === 'create' || action === 'c')) {
      if (actionPacket.params.room) {
        if (actionPacket.params.room === 'here') {
          targetID = c.socket.config.user.currentRoom.id;
        } else {
          targetID = parseInt(actionPacket.params.room.match(/(\d+)$/)[0], 10);
        }
        targetRoom = database.room.exists(db, targetID);
        isOwner = c.actionHelper._doesOwn(c, {
          type: CONST.TARGET_TYPE.ROOM,
          target: targetRoom
        });
      }
    }

    if (!action) {
      let roomList = database.room
        .getAllForOwner(db, c.socket.config.user.id)
        .map(item => {
          return `${item.name} (#${item.id})`;
        })
        .join(', ');
      msg(`<span class='highlight'>Your Rooms:</span> ${roomList}`);
      return;
    }

    switch (action) {
      case 'create':
      case 'c': {
        let newRoomID = database.room.create(db, {
          name: actionPacket.params.room,
          owner_id: c.socket.config.user.id
        });
        let room = database.room.getById(db, newRoomID);
        if (room) {
          return c.actionHelper.moveToRoom(c, room);
        }
        break;
      }

      case 'delete':
      case 'del':
      case 'd': {
        if (!targetRoom) {
          msg(`That room does not exist`);
        } else {
          if (!(isWizard | isOwner)) {
            msg(`You cannot delete a room you don't own.`);
          } else {
            msg(`Room destroyed!`);
            c.actionHelper._sendEveryoneInRoomHome(c, targetRoom);
            database.room.destroy(db, targetRoom.id);
          }
        }
        break;
      }

      case 'examine':
      case 'ex':
      case 'e': {
        if (!targetRoom) {
          msg(`That room does not exist`);
        } else {
          if (!(isWizard | isOwner)) {
            msg(`You cannot examine a room you don't own.`);
          } else {
            msg(`${JSON.stringify(database.room.getById(db, targetRoom.id))}`);
          }
        }
        break;
      }

      case 'set':
        if (!(isWizard | isOwner)) {
          msg(`You cannot modify a room you don't own.`);
        } else if (((actionPacket || {}).params || {}).option) {
          let option = actionPacket.params.option.toLowerCase().trim();
          switch (option) {
            case 'public':
              msg(`${targetRoom.name} is now public.`);
              database.room.update(db, targetRoom.id, { is_public: 1 });
              break;

            case 'disallow':
            case 'allow': {
              if (!actionPacket.params.option2) return;
              let listName = actionPacket.params.option2.toLowerCase().trim();
              let targetList = database.list.exists(
                db,
                listName,
                c.socket.config.user.id
              );
              if (targetList) {
                if (option === 'allow') {
                  msg(`${targetRoom.name} allow`);
                  database.room.update(db, targetRoom.id, {
                    whitelist_id: targetList.id
                  });
                } else {
                  msg(`${targetRoom.name} disallow`);
                  database.room.update(db, targetRoom.id, {
                    blacklist_id: targetList.id
                  });
                }
                break;
              } else {
                msg(`${listName} does not exist`);
              }
              break;
            }

            default:
              console.log(`I don't know how to ${option} `);
              return;
          }
          c.actionHelper._reconnectEveryoneInRoom(
            c,
            targetRoom,
            CONST.DO_QUIETLY
          );
        }
        break;

      case 'unset':
        if (!(isWizard | isOwner)) {
          msg(`You cannot modify a room you don't own.`);
        } else if (((actionPacket || {}).params || {}).option) {
          let option = actionPacket.params.option.toLowerCase().trim();
          switch (option) {
            case 'public':
              msg(`${targetRoom.name} is no longer public.`);
              database.room.update(db, targetRoom.id, { is_public: 0 });
              break;

            case 'disallow':
              msg(`blacklist cleared.`);
              database.room.update(db, targetRoom.id, {
                blacklist_id: null
              });
              break;

            case 'allow': {
              msg(`whitelist cleared.`);
              database.room.update(db, targetRoom.id, {
                whitelist_id: null
              });
              break;
            }

            default:
              console.log(`I don't know how to ${option} `);
              return;
          }
          c.actionHelper._reconnectEveryoneInRoom(
            c,
            targetRoom,
            CONST.DO_QUIETLY
          );
        }
        break;

      case 'chown':
        if (!(isWizard | isOwner)) {
          msg(`You cannot modify a room you don't own.`);
        } else if (((actionPacket || {}).params || {}).option) {
          let option = actionPacket.params.option.toLowerCase().trim();

          let newOwner = database.user.getByName(db, option);
          if (newOwner) {
            msg(
              `${targetRoom.name} now belongs to ${actionPacket.params.option}`
            );
            database.room.changeOwnership(db, targetRoom.id, newOwner.id);
            c.actionHelper._reconnectEveryoneInRoom(
              c,
              targetRoom,
              CONST.DO_QUIETLY
            );
          } else {
            return msg(`Cannot find user "${actionPacket.params.option}"?`);
          }
        }
        break;

      default:
        return;
    }
  },

  quit: c => {
    c.actionHelper._user_announce(c, 'Goodbye!');
    c.io.sockets.sockets[c.socket.id].disconnect();
  },

  reauthenticate: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    jwt.verify(
      actionPacket.action.params.token,
      process.env.JWT_SECRET,
      (err, decoded) => {
        if (err) console.log('JWT REAUTH FAIL');
        let lastLogin = moment().toISOString();
        let arrivingUser = database.user.getById(db, decoded.id);
        database.user.update(db, arrivingUser.name, {
          date_last_login: lastLogin
        });
        module.exports._onValidlogin(c, arrivingUser);
      }
    );
  },

  say: (c, actionPacket) => {
    return c.actionHelper._room_action(c, actionPacket);
  },

  teleport: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    let room = database.room.getById(db, actionPacket.params.id);
    if (room) {
      return c.actionHelper.moveToRoom(c, room);
    } else {
      c.actionHelper._user_announce(
        c,
        `Room #${actionPacket.params.id} does not exist.`
      );
    }
  },

  user: (c, actionPacket) => {
    let db = c.dbMgr.initialConfig.db;
    const msg = content => c.actionHelper._user_announce(c, content);
    const alreadyExists = () =>
      msg(`User "${actionPacket.params.username}" already exists!`);
    const doesNotExist = () =>
      msg(`User "${actionPacket.params.username}" does not exist!`);
    if (
      util.missingValue(actionPacket.params.username) |
      util.missingValue(actionPacket.params.action)
    ) {
      return msg(actionPacket.action.description);
    }

    let targetUser = database.user.getByName(db, actionPacket.params.username);
    let userAction = actionPacket.params.action.toLowerCase().trim();

    switch (userAction) {
      case 'create':
        if (targetUser === null) {
          database.user.create(
            db,
            {
              name: actionPacket.params.username,
              description: 'A shiny new entity!'
            },
            wasCreated => {
              if (wasCreated) {
                return msg(
                  `Created: user:${wasCreated.name} password:${wasCreated.password}`
                );
              } else {
                return alreadyExists;
              }
            }
          );
        } else {
          return alreadyExists;
        }
        break;

      case 'toad':
        if (targetUser === null) {
          return doesNotExist();
        } else if (c.socket.config.user.id == targetUser.id) {
          return msg(`You cannot toad yourself! Get a friend to do it.`);
        } else {
          database.user.update(db, targetUser.id, {
            role_id: CONST.ROLE.TOAD
          });
          c.actionHelper._updateUserOnMu(
            c,
            targetUser.id,
            'role_id',
            CONST.ROLE.TOAD
          );
          return msg(`${targetUser.name} is now a hideous toad!`);
        }

      case 'untoad':
      case 'unwiz':
        if (targetUser === null) {
          return doesNotExist();
        } else if (c.socket.config.user.id == targetUser.id) {
          return msg(
            `It is unwise to de-wizard yourself! Get a friend to do it.`
          );
        } else {
          database.user.update(db, targetUser.id, {
            role_id: CONST.ROLE.PLAYER
          });
          c.actionHelper._updateUserOnMu(
            c,
            targetUser.id,
            'role_id',
            CONST.ROLE.PLAYER
          );
          return msg(`${targetUser.name} is now a regular player.`);
        }

      case 'wiz':
        if (targetUser === null) {
          return doesNotExist();
        } else {
          database.user.update(db, targetUser.id, {
            role_id: CONST.ROLE.WIZARD
          });
          c.actionHelper._updateUserOnMu(
            c,
            targetUser.id,
            'role_id',
            CONST.ROLE.WIZARD
          );
          return msg(`${targetUser.name} is now a powerful wizard!`);
        }

      case 'destroy':
        if (targetUser === null) {
          return doesNotExist();
        } else if (c.socket.config.user.id == targetUser.id) {
          return msg(`You cannot destroy yourself! Get a friend to do it.`);
        } else {
          database.user.destroy(db, targetUser.name);
          //FIXME: Should kick
          return msg(`${targetUser.name} is gone forever.`);
        }

      case 'repass':
        //FIXME: Should kick
        if (targetUser === null) {
          return doesNotExist();
        } else {
          database.user.resetPassword(db, targetUser, newPassword => {
            if (newPassword !== null) {
              return msg(
                `${newPassword.name}'s new password: ${newPassword.password}`
              );
            }
          });
        }
        break;

      default:
        console.error(`I don't know how to '${userAction}' a user `);
    }
  },

  _onValidlogin: (c, user) => {
    user = { ...user, token: jwt.sign(user, process.env.JWT_SECRET) };
    let db = c.dbMgr.initialConfig.db;

    c.socket.config.user = {
      ...c.socket.config.user,
      ...user
    };

    c.socket.emit(CONST.MSG_TYPE.INFO, {
      date: Date.now(),
      payload: {
        user: c.socket.config.user,
        room: {
          id: 1
        },
        world: {
          actions: database.world.actionsForRole(db, user.role_id)
        }
      }
    });

    let home = user.home !== null ? user.home : database.room.getById(db, 1);
    c.actionHelper._user_announce(c, `Welcome ${user.name}!`);
    c.actionHelper._kickDuplicates(c);
    c.actionHelper.moveToRoom(c, home);
  },

  _parameterChange: (c, target, parameter, newValue, successMessage) => {
    let db = c.dbMgr.initialConfig.db;
    let isWizard = database.user.isWizard(db, c.socket.config.user.id);
    let isOwner = c.actionHelper._doesOwn(c, target);
    const msg = content => c.actionHelper._user_announce(c, content);
    let announce = message => {
      c.actionHelper._user_announce(c, message);
      c.actionHelper._room_announce(
        c,
        c.socket.config.user.currentRoom,
        message
      );
    };

    if (isWizard || isOwner) {
      switch (target.type) {
        case CONST.TARGET_TYPE.USER: {
          c.actionHelper._updateUserOnMu(
            c,
            target.target.id,
            parameter,
            newValue
          );
          database.user.update(db, target.target.id, {
            [parameter]: newValue
          });
          socketHelper.updateUserInfo(c, c.socket.config.user.currentRoom);
          announce(successMessage);
          return;
        }

        case CONST.TARGET_TYPE.ROOM:
        case CONST.TARGET_TYPE.LINK: {
          if (target.type === CONST.TARGET_TYPE.LINK) {
            database.link.update(db, target.target.id, {
              [parameter]: newValue
            });
          } else {
            database.room.update(db, target.target.id, {
              [parameter]: newValue
            });
          }
          announce(successMessage);
          c.actionHelper._reconnectEveryoneInRoom(
            c,
            target.target,
            CONST.DO_QUIETLY
          );
          return;
        }

        default: {
          console.error(`PARAM CHANGE: Unknown type ${target.target.type}`);
        }
      }
    } else {
      if (target.type === CONST.TARGET_TYPE.USER) {
        msg(`You cannot just change others, try asking nicely!`);
      } else {
        msg(`You cannot change things you don't own.`);
      }
    }
  }
};
