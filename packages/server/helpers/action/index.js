const CONST = require('const.js');
const command = require('./commands.js');
const verb = require('./verbs.js');
const jwt = require('jsonwebtoken');
const util = require('helpers/util.js');
const socketHelper = require('helpers/socket.js');
const database = require('helpers/db');

module.exports = {
  verb: verb,
  command: command,

  onIncomingAction: (connection, actionPacket) => {
    module.exports._checkForRequiredRole(
      connection,
      actionPacket,
      actionAllowed => {
        if (actionAllowed) {
          let isCommand = actionPacket.action.name.substr(0, 1) === '@';
          let actionName = isCommand
            ? actionPacket.action.name
                .split(',')[0]
                .substr(1, actionPacket.action.name.split(',')[0].length)
            : actionPacket.action.name.split(',')[0];
          if (module.exports.command[actionName] !== undefined) {
            module.exports.command[actionName](
              { ...connection, actionHelper: module.exports },
              actionPacket
            );
          } else if (module.exports.verb[actionName] !== undefined) {
            module.exports.verb[actionName](
              { ...connection, actionHelper: module.exports },
              actionPacket
            );
          } else {
            console.error(`No handler for action: '${actionName}'`);
          }
        }
      }
    );
  },

  moveToRoom: (connection, newRoom, quietly = false, cb) => {
    if (util.missingValue(newRoom.id)) {
      module.exports._user_announce(connection, 'No such room');
    } else if (!module.exports._allowedToEnter(connection, newRoom)) {
      let content = `You are not allowed to enter ${newRoom.name}!`;
      module.exports._user_announce(connection, `${content}`);
    } else {
      let oldRoom = connection.socket.config.user.currentRoom;
      connection.socket.leave(
        connection.socket.config.user.currentRoom.id,
        () => {
          if (!quietly) {
            let msg = `${connection.socket.config.user.name} leaves, heading towards '${newRoom.name}'.`;
            module.exports._room_announce(connection, oldRoom, msg);
          }
          socketHelper.updateUserInfo(connection, oldRoom);
          connection.socket.join(newRoom.id, () => {
            connection.socket.config.user.currentRoom = newRoom;
            module.exports.updateUsersCurrentRoom(connection, newRoom);
            if (!quietly) {
              let msg = `${connection.socket.config.user.name} has arrived from '${oldRoom.name}'`;
              module.exports._room_announce(connection, newRoom, msg);
            }
            socketHelper.updateUserInfo(connection, newRoom);
            if (!quietly) {
              command.do_look({ ...connection, actionHelper: module.exports });
            }
            if (typeof cb === 'function') {
              cb();
            }
          });
        }
      );
    }
  },

  _allowedToEnter: (c, room) => {
    let db = c.dbMgr.initialConfig.db;
    let userID = c.socket.config.user.id;
    let isWizard = database.user.isWizard(c.dbMgr.initialConfig.db, userID);
    let doesOwn = module.exports._doesOwn(c, {
      type: CONST.TARGET_TYPE.ROOM,
      target: room
    });
    if (isWizard || doesOwn) {
      return true;
    } else {
      let shouldAllow = room.is_public === 1;
      if (room.whitelist_id) {
        let whitelist = database.list.members(db, room.whitelist_id).members;
        if (
          whitelist.find(member => {
            return member.id === userID;
          })
        ) {
          shouldAllow = true;
        }
      }
      if (room.blacklist_id) {
        let blacklist = database.list.members(db, room.blacklist_id).members;
        if (
          blacklist.find(member => {
            return member.id === userID;
          })
        ) {
          shouldAllow = false;
        }
      }

      return shouldAllow;
    }
  },

  _doesOwn: (connection, target) => {
    let db = connection.dbMgr.initialConfig.db;
    switch (target.type) {
      case CONST.TARGET_TYPE.USER:
        return connection.socket.config.user.id === target.target.id;
      case CONST.TARGET_TYPE.ROOM:
        return database.room.checkOwnership(
          db,
          target.target.id,
          connection.socket.config.user.id
        );
      case CONST.TARGET_TYPE.LINK:
        return database.link.checkOwnership(
          db,
          target.target.id,
          connection.socket.config.user.id
        );
      default:
        console.error(`DOESOWN?: Unknown type ${target.type}`);
        console.log(target);
    }
  },

  _checkForRequiredRole: (connection, actionPacket, cb) => {
    const actionRequiresLogin =
      connection.dbMgr.initialConfig.actions[actionPacket.action.name] <
      CONST.ROLE.NOT_LOGGED_IN;
    if (actionRequiresLogin) {
      jwt.verify(actionPacket.token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) cb(false);
        connection.socket.config.user = {
          ...connection.socket.config.user,
          role_id: decoded.role_id,
          id: decoded.id
        };
        cb(
          decoded.role_id <=
            connection.dbMgr.initialConfig.actions[actionPacket.action.name]
        );
      });
    } else {
      cb(true);
    }
  },

  _kickDuplicates: connection => {
    let currentSocket = connection.socket;
    let currentUser = currentSocket.config.user;
    module.exports._forEachUserOnline(connection, (user, socket) => {
      if (user.id == currentUser.id && socket.id !== currentSocket.id) {
        socket.emit(CONST.MSG_TYPE.ACTION, {
          date: Date.now(),
          message: {
            sender: '',
            action: { name: 'emote' },
            text:
              'You have been disconnected because you logged in somewhere else.'
          }
        });
        socket.disconnect();
      }
    });
  },

  _forEachUserOnline: (connection, cb) => {
    Object.keys(connection.io.sockets.sockets).forEach(socketID => {
      let thisSocket = connection.io.sockets.sockets[socketID];
      cb(thisSocket.config.user, thisSocket);
    });
  },

  _parseName: string => {
    string = string.replace(/[~`!@#$%^&*(){}[\];:"'<,.>?/\\|_+=-]/g, '');
    if (
      typeof string === 'undefined' ||
      string.trim().length === 0 ||
      ['wizard', '"', "''"].includes(string.trim().toLowerCase())
    ) {
      return;
    }
    return string.trim();
  },

  _user_announce: (connection, message) => {
    connection.socket.emit(CONST.MSG_TYPE.ACTION, {
      date: Date.now(),
      message: {
        sender: '',
        action: { name: 'emote' },
        text: `<span class="system">${message}</span>`
      }
    });
  },

  _user_action: (connection, actionPacket) => {
    connection.socket.emit(CONST.MSG_TYPE.ACTION, {
      date: Date.now(),
      message: {
        ...actionPacket,
        sender: connection.socket.config.user
      }
    });
  },

  _room_announce: (connection, room, message) => {
    connection.socket.to(room.id).emit(CONST.MSG_TYPE.ACTION, {
      date: Date.now(),
      message: {
        sender: '',
        action: { name: 'emote' },
        text: `<span class="system">${message}</span>`
      }
    });
  },

  _room_action: (connection, actionPacket) => {
    connection.io
      .to(connection.socket.config.user.currentRoom.id)
      .emit(CONST.MSG_TYPE.ACTION, {
        date: Date.now(),
        message: {
          ...actionPacket,
          sender: connection.socket.config.user
        }
      });
  },

  _findTarget: targets => {
    let target = null;
    let type;
    if (targets.matchesFound == 1) {
      if (targets.rooms.length > 0) {
        type = CONST.TARGET_TYPE.ROOM;
        target = targets.rooms[0];
      } else if (targets.links.length > 0) {
        type = CONST.TARGET_TYPE.LINK;
        target = targets.links[0];
      } else if (targets.users.length > 0) {
        type = CONST.TARGET_TYPE.USER;
        target = targets.users[0];
      }
      target = { type, target };
    }
    return target;
  },

  _findTargetInRoom: (
    connection,
    target = 'here',
    scope = CONST.TARGET_TYPE.ALL
  ) => {
    let room = connection.socket.config.user.currentRoom;
    let matches = {
      searchterm: target,
      matchesFound: 0,
      users: [],
      rooms: [],
      links: []
    };

    if (typeof target !== 'undefined') {
      target = target.trim().toLowerCase();
      if (target === 'me') {
        matches.matchesFound++;
        matches.users.push(room.user);
      } else if (target === 'here') {
        matches.matchesFound++;
        matches.rooms.push(room);
      } else {
        if (
          scope === CONST.TARGET_TYPE.USER ||
          scope === CONST.TARGET_TYPE.ALL
        ) {
          let users = [];
          for (let socketID in connection.io.nsps['/'].adapter.nsp.sockets) {
            let socket = connection.io.nsps['/'].adapter.nsp.sockets[socketID];
            if (socket.config.user.currentRoom.id === room.id) {
              socket.config.user.socketID = socket.id;
              users.push(socket.config.user);
            }
          }

          if (users) {
            users.forEach(user => {
              if (user.name.toLowerCase().includes(target)) {
                matches.matchesFound++;
                matches.users.push(user);
              }
            });
          }
        }
        if (
          scope === CONST.TARGET_TYPE.LINK ||
          scope === CONST.TARGET_TYPE.ALL
        ) {
          if (room.links) {
            room.links.forEach(link => {
              if (link.name.toLowerCase().includes(target)) {
                matches.matchesFound++;
                matches.links.push(link);
              }
            });
          }
        }
      }
    }
    matches.exactMatch = matches.links.find(link => {
      return link.name.toLowerCase() === matches.searchterm;
    });
    return matches;
  },

  _sendEveryoneInRoomHome: (connection, room, quietly) => {
    let defaultHome = database.room.getById(
      connection.dbMgr.initialConfig.db,
      1
    );
    module.exports._forEachUserOnline(connection, (user, socket) => {
      if (user.currentRoom.id == room.id) {
        console.log(`Checking ${socket.id} ${user.name}`);
        console.log(`Sending ${user.name} home`);
        let home = user.home ? user.home : defaultHome;
        connection.actionHelper.moveToRoom(
          { ...connection, socket: socket },
          home,
          quietly
        );
      }
    });
  },

  _updateUserOnMu: (connection, id, parameter, newValue) => {
    module.exports._forEachUserOnline(connection, (user, socket) => {
      if (socket.config.user.id == id) {
        socket.config.user[parameter] = newValue;
        socket.emit(CONST.MSG_TYPE.INFO, {
          date: Date.now(),
          payload: {
            user: user
          }
        });
      }
    });
  },

  _reconnectEveryoneInRoom: (connection, room, quietly) => {
    let updatedRoom = database.room.getById(
      connection.dbMgr.initialConfig.db,
      room.id
    );
    module.exports._forEachUserOnline(connection, (user, socket) => {
      if (user.currentRoom.id == updatedRoom.id) {
        user.currentRoom = updatedRoom;
        connection.actionHelper.moveToRoom(
          { ...connection, socket: socket },
          user.currentRoom,
          quietly
        );
      }
    });
  },

  updateUsersCurrentRoom: (connection, room) => {
    connection.socket.emit(CONST.MSG_TYPE.INFO, {
      date: Date.now(),
      payload: {
        room
      }
    });
  }
};
