const jwt = require('jsonwebtoken');
const moment = require('moment');
const cors = require('cors');

module.exports = {
  setup: app => {
    app.use(cors());
    app.get('/', (req, res) => {
      res.sendFile(__dirname + '/../public/index.html');
    });

    app.get('/backup', module.exports._tokenRequired, (req, res) => {
      res.download(__dirname + '/../database/backup.db', `backup.db`);
    });
  },

  consoleWelcome: dbMgr => {
    let counts = `${dbMgr.initialConfig.world.userCount} Users / ${dbMgr.initialConfig.world.roomCount} Rooms`;
    console.log('================================');
    console.log(
      `==== Port: ${process.env.PORT} '${dbMgr.initialConfig.world.name}' online!`
    );
    console.log(`==== ${counts}`);
    console.log('================================');

    if (dbMgr.initialConfig.world.userCount === 0) {
      dbMgr.createDefaultAdminAccount(dbMgr.db, newWizard => {
        console.log('================================');
        console.log(`***  Empty World! Initializing God...`);
        console.log(`***  ${newWizard.name}:${newWizard.password}`);
        console.log('================================');
      });
    }
  },

  exitHandler: (process, dbMgr, server) => {
    process.stdin.resume();
    const exitHandler = options => {
      dbMgr.db.close();
      server.httpServer.close();
      if (options.exit) process.exit();
    };
    process.on('exit', exitHandler.bind(null, { cleanup: true }));
    process.on('SIGINT', exitHandler.bind(null, { exit: true }));
    process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
    process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));
    process.on('uncaughtException', err => {
      console.log(`CRASH ${moment(Date.now()).format(`DD/MM/YY @ h:MM:ssa`)}:`);
      console.log('======================================================');
      console.log(err);
      exitHandler({ exit: true });
    });
  },

  _tokenRequired: (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (!token) return res.status(404).send('');
    if (token.startsWith('Bearer ')) {
      token = token.slice(7, token.length);
    }
    jwt.verify(token, process.env.JWT_SECRET, err => {
      if (err) res.status(404).send('');
      next();
    });
  }
};
