module.exports = {
  create: (db, linkInfo) => {
    db.prepare('INSERT INTO lists(name, owner_id) VALUES (?,?)').run(
      linkInfo.name,
      linkInfo.owner_id
    );
  },

  getByNameAndOwnerID: (db, name, id) => {
    return db
      .prepare('SELECT * FROM lists WHERE name = ? AND owner_id = ?')
      .get(name, id);
  },

  getById: (db, id) => {
    return db.prepare('SELECT * FROM lists WHERE id = ?').get(id);
  },

  getAllForOwner: (db, id) => {
    let found = db.prepare('SELECT * FROM lists WHERE owner_ID = ?').all(id);
    return found;
  },

  members: (db, listID) => {
    let query = db
      .prepare(
        'SELECT lists.name as list_name, lists.id as list_id, users.name, users.id as user_id  FROM lists JOIN user_list ON user_list.list_id=lists.id JOIN users ON user_list.user_id=users.id WHERE lists.id = ?'
      )
      .all(listID);

    let result = { members: [], count: 0 };
    query.forEach(row => {
      result.list = row.list_name;
      result.id = row.list_id;
      result.count++;
      result.members.push({ name: row.name, id: row.user_id });
    });
    return result;
  },

  addMember: (db, listID, userID) => {
    db.prepare('INSERT INTO user_list(list_id, user_id) VALUES (?,?)').run(
      listID,
      userID
    );
  },

  removeMember: (db, listID, userID) => {
    db.prepare('DELETE FROM user_list WHERE list_id = ? AND user_id = ?').run(
      listID,
      userID
    );
  },

  update: (db, id, linkInfo) => {
    let existingInfo = module.exports.exists(db, id);
    if (!existingInfo) {
      console.error('list does not exist');
    } else {
      let newListInfo = {
        ...existingInfo,
        ...linkInfo
      };
      db.prepare('UPDATE lists SET name = ?, owner_id = ? WHERE id = ?').run(
        newListInfo.name,
        newListInfo.owner_id,
        id
      );
      return module.exports.getById(db, id);
    }
  },

  destroy: (db, name, owner_id) => {
    let deletedList = db
      .prepare('DELETE FROM lists WHERE name = ? AND owner_id = ?')
      .run(name, owner_id);
    db.prepare('DELETE FROM user_list WHERE list_id = ?').run(
      deletedList.lastInsertRowid
    );
  },

  exists: (db, listname, owner_id) => {
    let found = module.exports.getByNameAndOwnerID(db, listname, owner_id);
    return found === null ? false : found;
  },

  checkOwnership: (db, linkID, userID) => {
    let found = db
      .prepare('SELECT * FROM lists WHERE id = ? AND owner_ID = ?')
      .get(linkID, userID);
    return typeof found === 'undefined' ? false : true;
  }
};
