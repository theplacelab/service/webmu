module.exports = {
  actionsForRole: (db, role_id) => {
    return db.prepare('SELECT * FROM actions WHERE role_id >= ?').all(role_id);
  }
};
