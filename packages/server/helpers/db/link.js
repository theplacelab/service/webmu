module.exports = {
  create: (db, linkInfo) => {
    let newRecord = db
      .prepare(
        'INSERT INTO links(name, description, destination_id, owner_id) VALUES (?,?,?,?)'
      )
      .run(
        linkInfo.name,
        linkInfo.description,
        linkInfo.destination_id,
        linkInfo.owner_id
      );

    db.prepare('INSERT INTO room_link(room_id, link_id) VALUES (?,?)').run(
      linkInfo.location_id,
      newRecord.lastInsertRowid
    );
  },

  getById: (db, id) => {
    return db.prepare('SELECT * FROM links WHERE id = ?').get(id);
  },

  update: (db, id, linkInfo) => {
    let existingLink = module.exports.exists(db, id);
    if (!existingLink) {
      console.error('link does not exist');
    } else {
      let newLinkInfo = {
        ...existingLink,
        ...linkInfo
      };
      db.prepare(
        'UPDATE links SET name = ?, description = ?, owner_id = ?, destination_id = ? WHERE id = ?'
      ).run(
        newLinkInfo.name,
        newLinkInfo.description,
        newLinkInfo.owner_id,
        newLinkInfo.destination_id,
        id
      );
      return module.exports.getById(db, id);
    }
  },

  destroy: (db, id) => {
    db.prepare('DELETE FROM links WHERE id = ?').run(id);
    db.prepare('DELETE FROM room_link WHERE link_id = ?').run(id);
  },

  exists: (db, id) => {
    let found = module.exports.getById(db, id);
    return found === null ? false : found;
  },

  checkOwnership: (db, linkID, userID) => {
    let found = db
      .prepare('SELECT * FROM links WHERE id = ? AND owner_ID = ?')
      .get(linkID, userID);
    return typeof found === 'undefined' ? false : true;
  }
};
