const bcrypt = require('bcrypt');
const saltRounds = 10;
const moment = require('moment');
const util = require('helpers/util.js');
const room = require('helpers/db/room.js');
const CONST = require('const.js');
module.exports = {
  create: (db, userInfo, cb) => {
    if (module.exports.exists(db, userInfo.name)) {
      cb(false);
    } else {
      let newPass = util.generatePassword(10);
      bcrypt.hash(newPass, saltRounds, (err, hash) => {
        if (err) {
          cb(err);
        } else {
          let newRecord = db
            .prepare(
              'INSERT INTO users(name, password, description, date_created) VALUES (?,?,?,?)'
            )
            .run(
              userInfo.name,
              hash,
              userInfo.description,
              moment().toISOString()
            );
          cb({
            id: newRecord.lastInsertRowid,
            name: userInfo.name,
            description: userInfo.description,
            role_id: userInfo.role_id,
            password: newPass
          });
        }
      });
    }
  },

  resetPassword: (db, userInfo, cb) => {
    let newPass = util.generatePassword(10);
    bcrypt.hash(newPass, saltRounds, (err, hash) => {
      if (!err) {
        db.prepare('UPDATE users SET password = ? WHERE id = ?').run(
          hash,
          userInfo.id
        );
        module.exports.update(db, userInfo.id, { password: hash });
        cb({
          name: userInfo.name,
          password: newPass
        });
      } else {
        cb(null);
      }
    });
  },

  getById: (db, id) => {
    let userInfo = db.prepare('SELECT * FROM users WHERE id = ?').get(id);
    if (typeof userInfo === 'undefined') {
      return null;
    } else {
      return { ...userInfo, home: room.getById(db, userInfo.home) };
    }
  },

  getByName: (db, username) => {
    let userInfo = db
      .prepare('SELECT * FROM users WHERE lower(name) = ?')
      .get(username.toLowerCase().trim());
    if (typeof userInfo === 'undefined') {
      return null;
    } else {
      return { ...userInfo, home: room.getById(db, userInfo.home) };
    }
  },

  update: (db, id, newUserInfo) => {
    let existingUser = module.exports.exists(db, id);
    if (!existingUser) {
      console.error('user does not exist');
    } else {
      newUserInfo = {
        ...existingUser,
        ...newUserInfo
      };
      try {
        db.prepare(
          'UPDATE users SET name = ?, description = ?, role_id = ?, date_last_login =?, home=? WHERE id = ?'
        ).run(
          newUserInfo.name,
          newUserInfo.description,
          newUserInfo.role_id,
          newUserInfo.date_last_login,
          typeof newUserInfo.home === 'object' && newUserInfo.home !== null
            ? newUserInfo.home.id
            : newUserInfo.home,
          id
        );
      } catch (e) {
        console.log(`ERROR ON UPDATE: ${e}`);
      }
      return module.exports.getByName(db, newUserInfo.name);
    }
  },

  destroy: (db, username) => {
    db.prepare('DELETE FROM users WHERE name = ?').run(username);
  },

  exists: (db, userIdentifier) => {
    if (isNaN(userIdentifier)) {
      let findUser = module.exports.getByName(db, userIdentifier);
      return findUser === null ? false : findUser;
    } else {
      let findUser = module.exports.getById(db, userIdentifier);
      return findUser === null ? false : findUser;
    }
  },

  isWizard: (db, id) => {
    let found = db
      .prepare('SELECT * FROM users WHERE id = ? AND role_id = ?')
      .get(id, CONST.ROLE.WIZARD);
    return typeof found === 'undefined' ? false : true;
  },

  checkOwnership: (db, userID) => {
    let found = db.prepare('SELECT * FROM users WHERE id = ?').get(userID);
    return typeof found === 'undefined' ? false : true;
  }
};
