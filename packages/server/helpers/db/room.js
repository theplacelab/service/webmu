module.exports = {
  create: (db, roomInfo) => {
    let newRecord = db
      .prepare('INSERT INTO rooms(name, description, owner_id) VALUES (?,?,?)')
      .run(roomInfo.name, roomInfo.description, roomInfo.owner_id);
    db.prepare('UPDATE rooms SET description = ? WHERE id = ?').run(
      `Room ID: ${newRecord.lastInsertRowid}`,
      newRecord.lastInsertRowid
    );
    return newRecord.lastInsertRowid;
  },

  getAllForOwner: (db, id) => {
    return db
      .prepare('SELECT * FROM rooms WHERE owner_id = ? ORDER BY rooms.name')
      .all(id);
  },

  getById: (db, id) => {
    let room = db.prepare('SELECT * FROM rooms WHERE id = ?').get(id);
    let links = db
      .prepare(
        'SELECT links.* FROM links JOIN room_link ON room_link.link_id = links.id WHERE room_link.room_id = ?'
      )
      .all(id);
    if (typeof room === 'undefined') {
      return null;
    } else {
      return { ...room, links };
    }
  },

  changeOwnership: (db, id, newOwnerID) => {
    module.exports.update(db, id, { owner_id: newOwnerID });
    db.prepare(
      'UPDATE links SET owner_id = ? WHERE id IN (SELECT link_id from room_link where room_id = ?)'
    ).run(newOwnerID, id);
  },

  update: (db, id, roomInfo) => {
    let existingRoom = module.exports.exists(db, id);
    if (!existingRoom) {
      console.error('room does not exist');
    } else {
      let newRoomInfo = {
        ...existingRoom,
        ...roomInfo
      };
      db.prepare(
        'UPDATE rooms SET name = ?, description = ?, owner_id = ?, is_public = ?, blacklist_id = ?, whitelist_id = ? WHERE id = ?'
      ).run(
        newRoomInfo.name,
        newRoomInfo.description,
        newRoomInfo.owner_id,
        newRoomInfo.is_public,
        newRoomInfo.blacklist_id,
        newRoomInfo.whitelist_id,
        id
      );
      return module.exports.getById(db, id);
    }
  },

  destroy: (db, id) => {
    db.prepare('DELETE FROM rooms WHERE id = ?').run(id);
    db.prepare('DELETE FROM room_link WHERE room_id = ?').run(id);
    db.prepare(
      'UPDATE links set destination_id = NULL WHERE destination_id = ?'
    ).run(id);
  },

  exists: (db, id) => {
    let found = module.exports.getById(db, id);
    return found === null ? false : found;
  },

  checkOwnership: (db, roomID, userID) => {
    let found = db
      .prepare('SELECT * FROM rooms WHERE id = ? AND owner_id = ?')
      .get(roomID, userID);
    return typeof found === 'undefined' ? false : true;
  }
};
