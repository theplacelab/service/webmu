const bcrypt = require('bcrypt');
const user = require('helpers/db/user.js');
const link = require('helpers/db/link.js');
const room = require('helpers/db/room.js');
const list = require('helpers/db/list.js');
const world = require('helpers/db/world.js');
const moment = require('moment');
const CONST = require('const.js');
const { uniqueNamesGenerator } = require('unique-names-generator');

module.exports = {
  user: user,
  link: link,
  room: room,
  world: world,
  list: list,

  createDefaultAdminAccount: (db, cb) => {
    let username = uniqueNamesGenerator();

    user.create(
      db,
      { name: username, description: 'A Powerful Wizard' },
      newAccount => {
        user.update(db, newAccount.id, { role_id: CONST.ROLE.WIZARD });
        cb(newAccount);
      }
    );
  },

  loadInitialConfig: db => {
    let allActions = db.prepare('SELECT * FROM actions').all();
    let actions_lut = {};
    allActions.forEach(action => {
      actions_lut[action.name.split(',')[0]] = action.role_id;
    });
    let result = {
      db: db,
      actions: actions_lut,
      room: room.getById(db, 0),
      world: {
        ...db.prepare('SELECT * FROM world').get(),
        actions: db
          .prepare('SELECT * FROM actions WHERE role_id >= ?')
          .all(CONST.ROLE.NOT_LOGGED_IN),
        roomCount: db.prepare('SELECT count(*) as count FROM rooms').get()
          .count,
        userCount: db.prepare('SELECT count(*) as count FROM users').get().count
      }
    };
    return result;
  },

  authenticate: (db, opt, cb) => {
    if (!user.exists(db, opt.username)) {
      console.error(`LOGIN ERROR: user '${opt.username}' does not exist`);
      cb(false);
    } else {
      let userInfo = user.getByName(db, opt.username);
      let hash = userInfo.password;
      bcrypt.compare(opt.password, hash, (err, res) => {
        if (err) {
          console.error(`LOGIN ERROR: '${opt.username}' '${err}'`);
          cb(false);
        } else {
          if (res) {
            let lastLogin = moment().toISOString();
            user.update(db, userInfo.name, {
              date_last_login: lastLogin
            });
            cb({
              ...userInfo,
              password: ''
            });
          } else {
            console.error(`LOGIN ERROR: '${opt.username}' bad password`);
            cb(false);
          }
        }
      });
    }
  }
};
