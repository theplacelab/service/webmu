const { uniqueNamesGenerator } = require('unique-names-generator');
const CONST = require('const.js');

module.exports = {
  onConnect: connection => {
    let configuration = connection.dbMgr.initialConfig;
    let config = {
      room: {
        ...configuration.room
      },
      world: {
        ...configuration.world,
        rooms: connection.socket.rooms
      },
      user: {
        ip: connection.socket.handshake.headers['x-real-ip'],
        arrived_at: Date.now(),
        name: uniqueNamesGenerator(),
        description: 'does not exist',
        role_id: CONST.ROLE.NOT_LOGGED_IN
      }
    };
    connection.socket.emit(CONST.MSG_TYPE.INFO, {
      date: Date.now(),
      payload: config
    });
    return config;
  },

  onDisconnect: connection => {
    connection.socket
      .to(connection.socket.config.user.currentRoom.id)
      .emit(CONST.MSG_TYPE.ACTION, {
        date: Date.now(),
        message: {
          sender: '',
          action: { name: 'emote' },
          text: `${connection.socket.config.user.name} disconnected`
        }
      });

    let usersInRoom = module.exports._usersInRoom(
      connection,
      connection.socket.config.user.currentRoom.name
    );

    connection.io
      .to(connection.socket.config.user.currentRoom.id)
      .emit(CONST.MSG_TYPE.INFO, {
        date: Date.now(),
        payload: {
          world: {
            userCount: Object.keys(connection.io.sockets.sockets).length
          },
          room: {
            userCount: usersInRoom.length,
            users: usersInRoom
          }
        }
      });
  },

  updateUserInfo: (connection, room) => {
    let usersInRoom = module.exports._usersInRoom(connection, room.id);
    connection.io.to(room.id).emit(CONST.MSG_TYPE.INFO, {
      date: Date.now(),
      payload: {
        room: {
          userCount: usersInRoom.length,
          users: usersInRoom
        }
      }
    });
    connection.io.emit(CONST.MSG_TYPE.INFO, {
      date: Date.now(),
      payload: {
        world: {
          userCount: Object.keys(connection.io.sockets.sockets).length,
          users: module.exports._usersInWorld(connection, room.id)
        }
      }
    });
  },

  _usersInRoom: (connection, id) => {
    let users = [];
    if (typeof connection.io.nsps['/'].adapter.rooms[id] !== 'undefined') {
      for (let socketID in connection.io.nsps['/'].adapter.nsp.sockets) {
        let socket = connection.io.nsps['/'].adapter.nsp.sockets[socketID];
        if (socket.config.user.currentRoom.id === id) {
          users.push(socket.config.user);
        }
      }
    }
    return users;
  },

  _usersInWorld: connection => {
    let users = [];
    for (let socketID in connection.io.nsps['/'].adapter.nsp.sockets) {
      let socket = connection.io.nsps['/'].adapter.nsp.sockets[socketID];
      users.push(socket.config.user);
    }
    return users;
  }
};
