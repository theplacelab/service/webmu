module.exports = {
  ROLE: Object.freeze({
    WIZARD: 0,
    ADMIN: 1,
    PLAYER: 2,
    TOAD: 3,
    NOT_LOGGED_IN: 999
  }),

  MSG_TYPE: Object.freeze({
    INFO: 'INFO',
    ACTION: 'ACTION'
  }),

  RETURN_DATA_TYPE: Object.freeze({
    JSON: 'JSON',
    HTML: 'HTML',
    LINK: 'LINK'
  }),

  TARGET: Object.freeze({
    INLINE: 'INLINE',
    OVERLAY: 'OVERLAY'
  }),

  TARGET_TYPE: Object.freeze({
    ALL: 'ALL',
    ROOM: 'ROOM',
    LINK: 'LINK',
    USER: 'USER'
  }),

  DO_QUIETLY: 'true'
};
